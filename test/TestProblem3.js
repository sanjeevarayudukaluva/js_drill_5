const employeesData = require("../js_drill_5");
const langauges = require("../Problem3.js");
try {
  const allLanguages = langauges.uniqueLanguages(employeesData.dataSet);
  if (
    allLanguages == null ||
    allLanguages === undefined ||
    allLanguages.length === 0
  ) {
    throw new Error("data not found or null");
  }
  console.log(allLanguages);
} catch (error) {
  console.log(error);
}
