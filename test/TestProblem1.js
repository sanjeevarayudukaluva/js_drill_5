const employeesData = require("../js_drill_5.js");
const nameEmailDomain = require("../Problem1.js");

try {
  const fullNameAndEmailDomain = nameEmailDomain.firstNameLastNameEmailDomain(
    employeesData.dataSet
  );
  if (fullNameAndEmailDomain === null || fullNameAndEmailDomain === undefined) {
    throw new Error("Data is empty or null");
  }
  console.log(fullNameAndEmailDomain);
} catch (error) {
  console.log(error);
}
