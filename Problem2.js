/*Check the each person projects list and group the data based on the project status and return the data as below
{
    'Completed': ["Mobile App Redesign","Product Launch Campaign","Logo Redesign","Project A".. so on],
    'Ongoing': ["Website User Testing","Brand Awareness Campaign","Website Mockup" .. so on]
    .. so on
  }*/

function projectStatus(data){
  if(Array.isArray(data)&&data.length!==0){
    const dataStatus={};
    data.forEach(eachObject => {
      eachObject.projects.forEach(element =>{
        if(dataStatus[element.status]){
          dataStatus[element.status].push(element.name);
        }
        else{
          dataStatus[element.status]=[];
        }
      })
    })
    return dataStatus;
  }
  else{
    return null;
  }
}
module.exports={projectStatus};
