function uniqueLanguages(data) {
  if (Array.isArray(data) && data.length !== 0) {
    const languages = [];
    data.forEach((eachObject) => {
      if (eachObject.languages && Array.isArray(eachObject.languages)) {
        eachObject.languages.forEach((element) => {
          if (!languages.includes(element)) {
            languages.push(element);
          }
        });
      }
    });
    return languages;
  } else {
    return null;
  }
}

module.exports = { uniqueLanguages };
