// Iterate over each object in the array and in the email breakdown the email and return the output as below:
function firstNameLastNameEmailDomain(data) {
  if (Array.isArray(data) && data.length !== 0) {
    return data.map((eachObject) => {
      const [name, emailDomain] = eachObject.email.split("@");
      const [firstName, lastName] = name.split(".").map(element => element.charAt(0).toUpperCase()+element.slice(1));
      return {
        firstName,
        lastName,
        emailDomain,
      };
    });
  } else {
    return null;
  }
}
module.exports={firstNameLastNameEmailDomain};